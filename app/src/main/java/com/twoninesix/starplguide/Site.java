package com.twoninesix.starplguide;

import android.content.Context;
import android.net.Uri;

import androidx.browser.customtabs.CustomTabsIntent;

import com.hk.smart.ads.utils.Prefs;

public class Site {
    public static void gamezop(Context activity){
        CustomTabsIntent.Builder builder1 = new CustomTabsIntent.Builder();
        CustomTabsIntent customTabsIntent = builder1.build();
        customTabsIntent.launchUrl(activity, Uri.parse(Prefs.getString("gamezop_id")));
    }
    public static void qureka(Context activity){
        CustomTabsIntent.Builder builder1 = new CustomTabsIntent.Builder();
        CustomTabsIntent customTabsIntent = builder1.build();
        customTabsIntent.launchUrl(activity, Uri.parse(Prefs.getString("qureka_id")));
    }
    public static void privacy(Context activity){
        CustomTabsIntent.Builder builder1 = new CustomTabsIntent.Builder();
        CustomTabsIntent customTabsIntent = builder1.build();
        customTabsIntent.launchUrl(activity, Uri.parse(Prefs.getString("privacy_url")));
    }
}
