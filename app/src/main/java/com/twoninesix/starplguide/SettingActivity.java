package com.twoninesix.starplguide;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hk.smart.ads.banner.BannerType;
import com.hk.smart.ads.banner.SmartBanner;
import com.hk.smart.ads.interstitial.SmartInterstitialPreload;
import com.hk.smart.ads.listeners.SmartAdListener;
import com.hk.smart.ads.nativead.SmartNative;
import com.hk.smart.dialogs.RateStarDialog;

public class SettingActivity extends AppCompatActivity {
    ImageView btn_back;
    EditText txt_name;
    SmartInterstitialPreload smartInterstitialPreload;
    RelativeLayout rl_update, rl_rate_us, rl_share, rl_privacy;
    TextView txt_version, txt_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        SmartBanner smartBanner = new SmartBanner(this);
        smartBanner.loadBanner(findViewById(R.id.banner_ad_bottom), BannerType.SMART_BANNER);
        SmartNative smartNative = new SmartNative(this);
        smartNative.loadNativeAd(findViewById(R.id.ad_view_container));
        smartNative.showNativeAd();
        smartInterstitialPreload = new SmartInterstitialPreload(this);
        txt_title = findViewById(R.id.txt_title);
        txt_title.setText("App Setting");
        txt_name = findViewById(R.id.txt_name);
        txt_version = findViewById(R.id.txt_version);
        String versionName = "1.0";
        int versionCode = 1;
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
            versionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        txt_version.setText(versionName + ", " + versionCode);


        rl_rate_us = findViewById(R.id.rl_rate_us);
        rl_rate_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new RateStarDialog(SettingActivity.this);
            }
        });

        rl_update = findViewById(R.id.rl_update);
        rl_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this, UpdateActivity.class));
            }
        });

        rl_share = findViewById(R.id.rl_share);
        rl_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
                shareIntent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.app_name) +
                        "\n\nClick to download:\n" +
                        "https://play.google.com/store/apps/details?id=" + getPackageName());
                startActivity(Intent.createChooser(shareIntent, "Share via"));
            }
        });

        rl_privacy = findViewById(R.id.rl_privacy);
        rl_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
 Site.privacy(getApplicationContext());
            }
        });

        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        smartInterstitialPreload.showInterstitial(new SmartAdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                SettingActivity.super.onBackPressed();
            }

            @Override
            public void onAdNotFound() {
                super.onAdNotFound();
                onAdClosed();
            }
        });
    }
}