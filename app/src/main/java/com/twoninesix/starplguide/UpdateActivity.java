package com.twoninesix.starplguide;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.hk.smart.ads.banner.BannerType;
import com.hk.smart.ads.banner.SmartBanner;
import com.hk.smart.ads.interstitial.SmartInterstitialPreload;
import com.hk.smart.ads.listeners.SmartAdListener;
import com.hk.smart.ads.nativead.SmartNative;

public class UpdateActivity extends AppCompatActivity {
    SmartInterstitialPreload smartInterstitialPreload;
    RelativeLayout rl_update;
    TextView txt_version, txt_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        SmartBanner smartBanner = new SmartBanner(this);
        smartBanner.loadBanner(findViewById(R.id.banner_ad_bottom), BannerType.SMART_BANNER);
        SmartNative smartNative = new SmartNative(this);
        smartNative.loadNativeAd(findViewById(R.id.ad_view_container));
        smartNative.showNativeAd();
        smartInterstitialPreload = new SmartInterstitialPreload(this);
        txt_title = findViewById(R.id.txt_title);
        txt_title.setText("App Update");
        txt_version = findViewById(R.id.txt_version);
        String versionName = "1.0";
        int versionCode = 1;
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
            versionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        txt_version.setText(versionName + ", " + versionCode);


        rl_update = findViewById(R.id.rl_update);
        rl_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog appupdate;
                appupdate = new Dialog(UpdateActivity.this, R.style.AdLoadingDialogTheme);
                appupdate.setContentView(R.layout.diag_update);
                appupdate.getWindow().setBackgroundDrawable(new ColorDrawable(UpdateActivity.this.getResources().getColor(R.color.dark_alpha70)));
                appupdate.setCancelable(false);
                appupdate.show();

                AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(UpdateActivity.this);

                // Returns an intent object that you use to makhna_check for an update.
                Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

                // Checks that the platform will allow the specified type of update.
                appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
                    if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {

                        if (!UpdateActivity.this.isDestroyed() && appupdate.isShowing())
                            appupdate.dismiss();

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(UpdateActivity.this);
                        alertDialogBuilder.setMessage("New update available. \n Features improvements.");
                        alertDialogBuilder.setPositiveButton("Update",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                        try {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                        } catch (android.content.ActivityNotFoundException anfe) {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                        }
                                    }
                                });

                        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                });

                appUpdateInfoTask.addOnFailureListener(e -> {

                    if (!UpdateActivity.this.isDestroyed() && appupdate.isShowing())
                        appupdate.dismiss();

                    Toast.makeText(UpdateActivity.this, "No update found", Toast.LENGTH_SHORT).show();
                });
            }
        });

        ImageView btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        smartInterstitialPreload.showInterstitial(new SmartAdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                UpdateActivity.super.onBackPressed();
            }

            @Override
            public void onAdNotFound() {
                super.onAdNotFound();
                onAdClosed();
            }
        });
    }
}