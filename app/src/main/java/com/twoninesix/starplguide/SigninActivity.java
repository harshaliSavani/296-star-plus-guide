package com.twoninesix.starplguide;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.hk.smart.ads.interstitial.SmartInterstitialPreload;
import com.hk.smart.ads.listeners.SmartAdListener;
import com.hk.smart.ads.nativead.SmartNative;
import com.hk.smart.ads.utils.ActivityHelper;
import com.hk.smart.ads.utils.Prefs;

public class SigninActivity extends AppCompatActivity {
    Button btn_continue;
    EditText txt_name, txt_Add, txt_Phone;
    RadioGroup radioGroup;
    RadioButton radio_male, radio_female;
    SmartNative smartNative;
    SmartInterstitialPreload smartInterstitialPreload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        smartNative = new SmartNative(this);
        smartNative.loadNativeAd(findViewById(R.id.ad_view_container));
        smartNative.showNativeAd();
        smartInterstitialPreload = new SmartInterstitialPreload(this);

        txt_name = findViewById(R.id.txt_name);
        txt_Add = findViewById(R.id.txt_Add);
        txt_Phone = findViewById(R.id.txt_Phone);
        radioGroup = findViewById(R.id.radio_group);
        radio_male = findViewById(R.id.radio_male);
        radio_female = findViewById(R.id.radio_female);
        String ddj = Prefs.getString("name");
        if (!ddj.equals("")) {
            txt_name.setText(Prefs.getString("name"));
            txt_Add.setText(Prefs.getString("add"));
            txt_Phone.setText(Prefs.getString("phone"));
        }
        btn_continue = findViewById(R.id.btn_continue);
        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Name = txt_name.getText().toString();
                String Add = txt_Add.getText().toString();
                String Phone = txt_Phone.getText().toString();
                if (Phone.length() == 10) {
                    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                    if (Add.matches(emailPattern)) {
                        Prefs.putString("name", Name);
                        Prefs.putString("add", Add);
                        Prefs.putString("phone", Phone);
                        if (Name.equals("")) {
                            Toast.makeText(SigninActivity.this, "Please enter your name.", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (Add.equals("")) {
                            Toast.makeText(SigninActivity.this, "Please enter your Address.", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (Phone.equals("")) {
                            Toast.makeText(SigninActivity.this, "Please enter your Phone.", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        if (radioGroup.getCheckedRadioButtonId() == -1) {
                            Toast.makeText(SigninActivity.this, "Please select gender.", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Prefs.putString(Constant.USER_NAME, txt_name.getText().toString());
                        if (radioGroup.getCheckedRadioButtonId() == R.id.radio_male) {
                            Prefs.putString(Constant.GENDER, "male");
                        } else {
                            Prefs.putString(Constant.GENDER, "female");
                        }

                        Prefs.putBoolean(Constant.IS_LOGGED_IN, true);

                        smartInterstitialPreload.showInterstitial(new SmartAdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        ActivityHelper.startActivity(SigninActivity.this, MainActivity.class, true);
                                    }
                                }, 100);
                            }

                            @Override
                            public void onAdNotFound() {
                                super.onAdNotFound();
                                onAdClosed();
                            }
                        });
                    } else {
                        Toast.makeText(getApplicationContext(), "Invalid email address", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(SigninActivity.this, "Please enter valide phonember.", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });
    }
}