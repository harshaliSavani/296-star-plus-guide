package com.twoninesix.starplguide;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.hk.smart.ads.banner.BannerType;
import com.hk.smart.ads.banner.SmartBanner;
import com.hk.smart.ads.interstitial.SmartInterstitialPreload;
import com.hk.smart.ads.listeners.SmartAdListener;

public class MenuActivity extends AppCompatActivity {

    private SmartInterstitialPreload smartInterstitialPreload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        SmartBanner smartBanner = new SmartBanner(this);
        smartBanner.loadBanner(findViewById(R.id.banner_ad_bottom), BannerType.SMART_BANNER);
        smartInterstitialPreload = new SmartInterstitialPreload(this);
        ImageView imgback = findViewById(R.id.btn_back);
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        LinearLayout btnplaygame = findViewById(R.id.btnplaygame);
        btnplaygame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Site.gamezop(getApplicationContext());
            }
        });
        Button btngame = findViewById(R.id.btngame);
        btngame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Site.qureka(getApplicationContext());
            }
        });
        LinearLayout btnstartgame = findViewById(R.id.btnstartgame);
        btnstartgame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Site.gamezop(getApplicationContext());
            }
        });
        LinearLayout btnwinplay = findViewById(R.id.btnwinplay);
        btnwinplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Site.qureka(getApplicationContext());
            }
        });
        LinearLayout btnplayandwin = findViewById(R.id.btnplayandwin);
        btnplayandwin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Site.gamezop(getApplicationContext());
            }
        });
        LinearLayout btnbanner = findViewById(R.id.btnbanner);
        btnbanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Site.qureka(getApplicationContext());
            }
        });
        LinearLayout btnguidefortv = findViewById(R.id.btnguidefortv);
        btnguidefortv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                smartInterstitialPreload.showInterstitial(new SmartAdListener() {
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        startActivity(new Intent(MenuActivity.this, MenuListActivity.class));
                    }

                    @Override
                    public void onAdNotFound() {
                        super.onAdNotFound();
                    }
                });
            }
        });
    }

    @Override
    public void onBackPressed() {
        smartInterstitialPreload.showInterstitial(new SmartAdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                finish();
            }

            @Override
            public void onAdNotFound() {
                super.onAdNotFound();
            }
        });
    }
}