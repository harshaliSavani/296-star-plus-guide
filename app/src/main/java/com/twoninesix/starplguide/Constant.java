package com.twoninesix.starplguide;

public class Constant {
    public static String IS_PRIVACY_ACCEPTED = "is_privacy_accepted";
    public static String IS_LOGGED_IN = "is_logged_in";
    public static String IS_PERMISSION = "is_permission";
    public static String USER_NAME = "user_name";
    public static String AGE = "age";
    public static String GENDER = "gender";
    public static String IS_CONSENT = "is_consent";
    public static String RATE_DONE = "rate_done";
    public static String IMPRESSION_MSG = "impression_msg";

    public static String CALL_COUNT = "call_count";
}
