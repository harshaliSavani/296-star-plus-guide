package com.twoninesix.starplguide;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.hk.smart.ads.banner.BannerType;
import com.hk.smart.ads.banner.SmartBanner;
import com.hk.smart.ads.interstitial.SmartInterstitialPreload;
import com.hk.smart.ads.listeners.SmartAdListener;

public class MenuListActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btn1, btn2, btn3, btn4, btn5;
    private SmartInterstitialPreload smartInterstitialPreload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_list);
        SmartBanner smartBanner = new SmartBanner(this);
        smartBanner.loadBanner(findViewById(R.id.banner_ad_bottom), BannerType.SMART_BANNER);
        smartInterstitialPreload = new SmartInterstitialPreload(this);
        ImageView imgback = findViewById(R.id.btn_back);
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        ImageView imgbanner = findViewById(R.id.imgbanner);
        imgbanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
          Site.gamezop(getApplicationContext());
            }
        });
        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);
        btn4 = findViewById(R.id.btn4);
        btn5 = findViewById(R.id.btn5);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        smartInterstitialPreload.showInterstitial(new SmartAdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                finish();
            }

            @Override
            public void onAdNotFound() {
                super.onAdNotFound();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn1:
                start(0);
                break;
            case R.id.btn2:
                start(1);
                break;
            case R.id.btn3:
                start(2);
                break;
            case R.id.btn4:
                start(3);
                break;
            case R.id.btn5:
                start(4);
                break;
        }
    }

    private void start(int i) {
        smartInterstitialPreload.showInterstitial(new SmartAdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                Intent intent = new Intent(MenuListActivity.this, DetailActivity.class);
                intent.putExtra("pos", i);
                startActivity(intent);
            }

            @Override
            public void onAdNotFound() {
                super.onAdNotFound();
                onAdClosed();
            }
        });
    }
}