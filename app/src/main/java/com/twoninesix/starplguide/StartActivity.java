package com.twoninesix.starplguide;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.hk.smart.ads.banner.BannerType;
import com.hk.smart.ads.banner.SmartBanner;
import com.hk.smart.ads.interstitial.SmartInterstitialPreload;
import com.hk.smart.ads.listeners.SmartAdListener;
import com.hk.smart.ads.nativead.SmartNative;
import com.hk.smart.dialogs.RateStarDialog;

public class StartActivity extends AppCompatActivity {

    private SmartInterstitialPreload smartInterstitialPreload;
    private Button btn_update, btn_start, btn_setting, btn_share, btn_rate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        SmartBanner smartBanner = new SmartBanner(this);
        smartBanner.loadBanner(findViewById(R.id.banner_ad_bottom), BannerType.SMART_BANNER);
        SmartNative smartNative = new SmartNative(this);
        smartNative.loadNativeAd(findViewById(R.id.ad_view_container));
        smartNative.showNativeAd();
        smartInterstitialPreload = new SmartInterstitialPreload(this);
        ImageView btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btn_start = findViewById(R.id.btn_start);
        btn_update = findViewById(R.id.btn_update);
        btn_setting = findViewById(R.id.btn_setting);
        btn_share = findViewById(R.id.btn_share);
        btn_rate = findViewById(R.id.btn_rate);
        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                smartInterstitialPreload.showInterstitial(new SmartAdListener() {
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        startActivity(new Intent(StartActivity.this, MenuActivity.class));
                    }

                    @Override
                    public void onAdNotFound() {
                        super.onAdNotFound();
                    }
                });

            }
        });
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                smartInterstitialPreload.showInterstitial(new SmartAdListener() {
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        startActivity(new Intent(StartActivity.this, UpdateActivity.class));
                    }

                    @Override
                    public void onAdNotFound() {
                        super.onAdNotFound();
                    }
                });
            }
        });
        btn_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                smartInterstitialPreload.showInterstitial(new SmartAdListener() {
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        startActivity(new Intent(StartActivity.this, SettingActivity.class));
                    }

                    @Override
                    public void onAdNotFound() {
                        super.onAdNotFound();
                    }
                });
            }
        });
        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent("android.intent.action.SEND");
                sharingIntent.setType("text/plain");
                String shareBody = ("https://play.google.com/store/apps/details?id=" + getPackageName() + "&hl=en");
                sharingIntent.putExtra("android.intent.extra.SUBJECT", "Subject Here");
                sharingIntent.putExtra("android.intent.extra.TEXT", shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });
        btn_rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new RateStarDialog(StartActivity.this);
            }
        });
    }

    @Override
    public void onBackPressed() {
        smartInterstitialPreload.showInterstitial(new SmartAdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                finish();
            }

            @Override
            public void onAdNotFound() {
                super.onAdNotFound();
            }
        });
    }
}