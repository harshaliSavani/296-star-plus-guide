package com.twoninesix.starplguide;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hk.smart.ads.interstitial.SmartInterstitialPreload;
import com.hk.smart.ads.listeners.SmartAdListener;
import com.hk.smart.ads.nativead.SmartNative;

public class DetailActivity extends AppCompatActivity {
    private SmartInterstitialPreload smartInterstitialPreload;
    String[] title = new String[]{"Introduction","How to Download Stra Plus Guide","Features of Stra Plus Guide for PC","Steps How To Connect Stra Plus Guide To Pc","Conclusion"};
    String[] txtdetail = new String[]{"Starplus is a platform to invest in anything, the more a crowd supports a topic, the more a topic is worth."
            ,"All the info you watch free Indian TV channels in Assamese, Bengali, Bhojpuri, English, Gujarati, Hindi, Kannada, Maithili, Malayalam, Marathi, Oriya, Punjabi, Tamil, Telugu and Urdu languages."
            ,"Star Plus Colors TV-Hotstar Live TV Guide!! General entertainment channel Hindi, the main language of Star Network. #1 Official TV station in the United Kingdom. The Asian TV destination in the UK, with top rated shows that include dramas, reality shows, international format shows. You can watch the most watched Hindi performances in India."
            ,"You can watch free TV channels divided in following categories: * Live Cricket Tv,Live TV, HD TV * News * Entertainment * Sports * Channel list * Educational and much more..."
            ,"Star Plus,Colors TV-Hotstar Live TV HD Guide is guide app to give you some suggestion to use star plus tv serial india. We provide a tutorial, how to and step to use star plus tv channel app in hindi. star plus serial gives live tv show, serial, dramas and the latest updates, free streaming of videos and lots of video highlights. And now, whether you want to watch movies or serials in Indian TV using Star Plus online, you can be assured that it's best quality online channel. Watch your favorite TV shows and upcoming series. Star Plus is your answer to watch live TV, movies and your favorite TV series and lets you enjoy it and all times."};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        SmartNative smartNative = new SmartNative(this);
        smartNative.loadNativeAd(findViewById(R.id.ad_view_container));
        smartNative.showNativeAd();
        smartInterstitialPreload = new SmartInterstitialPreload(this);
        ImageView btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        ImageView imgbanner=findViewById(R.id.imgbanner);
        imgbanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
          Site.qureka(getApplicationContext());
            }
        });
        int pos = getIntent().getIntExtra("pos", 0);
        TextView txttitle = findViewById(R.id.txttitle);
        TextView txt = findViewById(R.id.txt);
        txttitle.setText(title[pos]);
        txt.setText(txtdetail[pos]);
    }
    @Override
    public void onBackPressed() {
        smartInterstitialPreload.showInterstitial(new SmartAdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                finish();
            }

            @Override
            public void onAdNotFound() {
                super.onAdNotFound();
            }
        });
    }
}