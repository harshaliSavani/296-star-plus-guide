package com.twoninesix.starplguide;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.hk.smart.ads.banner.BannerType;
import com.hk.smart.ads.banner.SmartBanner;
import com.hk.smart.ads.interstitial.SmartInterstitialPreload;
import com.hk.smart.ads.listeners.SmartAdListener;
import com.hk.smart.ads.nativead.SmartNative;

public class MainActivity extends AppCompatActivity {

    private SmartInterstitialPreload smartInterstitialPreload;
    private Button btn_start, btnplayandwin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SmartBanner smartBanner = new SmartBanner(this);
        smartBanner.loadBanner(findViewById(R.id.banner_ad_bottom), BannerType.SMART_BANNER);
        SmartNative smartNative = new SmartNative(this);
        smartNative.loadNativeAd(findViewById(R.id.ad_view_container));
        smartNative.showNativeAd();
        smartInterstitialPreload = new SmartInterstitialPreload(this);
        ImageView btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btn_start = findViewById(R.id.btn_start);
        btnplayandwin = findViewById(R.id.btnplayandwin);
        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                smartInterstitialPreload.showInterstitial(new SmartAdListener() {
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        startActivity(new Intent(MainActivity.this, StartActivity.class));
                    }

                    @Override
                    public void onAdNotFound() {
                        super.onAdNotFound();
                    }
                });
            }
        });
        btnplayandwin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                smartInterstitialPreload.showInterstitial(new SmartAdListener() {
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                      Site.qureka(getApplicationContext());
                    }

                    @Override
                    public void onAdNotFound() {
                        super.onAdNotFound();
                    }
                });
            }
        });
    }

    @Override
    public void onBackPressed() {
        smartInterstitialPreload.showInterstitial(new SmartAdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                finish();
            }

            @Override
            public void onAdNotFound() {
                super.onAdNotFound();
            }
        });
    }
}